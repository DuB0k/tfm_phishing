 Detector de phishing mediante Machine Learning 
===
Este repositorio contiene el c�digo de un programa desarrollado en Python para la detecci�n de sitios web con Phishing utilizando la libreria [scikit-learn](https://scikit-learn.org/stable/) para el aprendizaje automatico supervisado.

El contenido del repositorio es el siguiente:

- **phishing_detector.py** 
C�digo fuente desarrollado para la deteccion de Phishing a traves de un algoritmo de Machine Learning.

- **README**
 Contiene la descripci�n del contenido del repositorio junto con las instrucciones de instalaci�n y ejecuci�n del programa.

- **requirements.txt**
Fichero con las dependencias necesarias para la instalaci�n del programa.

- **full_legit.csv**
Fichero con direcciones URL leg�timas necesarias para la generaci�n de un nuevo modelo.

- **1909_verified_online.csv**
Fichero con direcciones URL con phishing necesarias para la generaci�n de un nuevo modelo.

- **model_google.pkl**
Modelo generado con 4290 URLs (2024 con phishing y 2266 leg�timas) con la caracter�stica googlesafe. 

- **model_normal.pkl**
Modelo generado con 18387 URLs (9415 con phishing y 8972 leg�timas). 
---
## Instalaci�n del programa
--------------

Este programa ha sido desarrollado para la versi�n de Python 3.7 aunque deberia ser compatible con versiones posteriores.

Para instalar el programa hay que lanzar el siguiente comando desde una ventana de consola:
    
    $ pip install -r requirements.txt

Para obtener ayuda para la ejecuci�n del programa hay que lanzarlo con la opci�n **-h** o **--help**:

    $ python phishing_detector.py -h


## Generaci�n de un modelo
--------------

Para generar un nuevo modelo de entrenamiento para el clasificador hay que invocar el programa con los siguientes par�metros:

 - **phishing_sites**
 Nombre del fichero CSV con las direcciones URLs con phishing. En el repositorio se ha incluido un fichero de ejemplo descargado desde [Phishtank](https://www.phishtank.com/) denominado *1909_verified_online.csv*.

 - **legit_sites**
 Nombre del fichero CSV con las direcciones URL legitimas. En el repositorio se ha incluido un fichero de ejemplo denominado *full_legit.csv*.

 - **features**
 Nombre del fichero donde se van a guardar el vector de caracter�sticas de las URLs analizadas durante el proceso de generaci�n del modelo. 

 - **generate_model**
 Este parametro admite dos valores: **normal** o **google**.
 Si se invoca el programa con *--generate_model normal* se realiza una extracci�n del vector de caracter�sitcas sin tener en cuenta la API de [Google Safe Browsing](https://developers.google.com/safe-browsing/v4/lookup-api). En caso de invocar el programa con *--generate_model google* es necesario incluir el par�metro **api_key**. 

 - **api_key**
 Clave de desarrollador que facilita Google para invocar al servicio [Google Safe Browsing](https://developers.google.com/safe-browsing/v4/lookup-api). Para obtener una API_KEY es necesario registrarse de manera gratuita en el progama de [desarrolladores de Google](https://console.developers.google.com/).

 - **max_urls**
N�mero m�ximo de direcciones URL a procesar para cada fichero CSV recibido como par�metro. Tiene un valor por defecto de 10.000. 

 El siguiente ejemplo muestra como generar un modelo sin tener en cuenta el servicio *GoogleSafeBrowsing*:

    $ python phishing_detector.py --phishing_sites 1909_verified_online.csv --legit_sites full_legit.csv --max_urls 2500 --features nuevo_modelo.pkl --generate_model normal

 El siguiente ejemplo muestra como generar un modelo utilizando el servicio *GoogleSafeBrowsing*:

    $ python phishing_detector.py --phishing_sites 1909_verified_online.csv --legit_sites full_legit.csv --max_urls 2500 --features nuevo_modelo_google.pkl --generate_model google --api_key <API_KEY>
---

## Evaluaci�n de un modelo

Para evaluar un modelo de clasificador es necesario haberlo generado previamente e invocar el programa con los siguientes par�metros:

 - **evaluate**
Este parametro admite dos valores: **normal** o **cross**. Si se utiliza el valor *--evaluate normal* el programa divide los datos del vector de caracter�sticas en un conjunto aleatorio de datos de entrenamiento con el 80% de las URL y utiliza el 20% de URLs restante para evaluar el desempe�o del clasificador. En el caso de utilizar el valor *--evaluate cross* se realiza una validacion cruzada (80% entrnamiento y 20% pruebas) con 5 subconjuntos aleatorios del vector de caracter�sticas.  

 - **features**
 Nombre del fichero que contiene el vector de caracter�sticas de todas las URLs analizadas y que se ha generado previamente mediante la opcion *--generate_model*.

El siguiente ejemplo muestra como evaluar el modelo **model_google.pkl** incluido en el repositorio y que se ha generado utilizando el servicio *GoogleSafeBrowsing*:
  
    $ python phishing_detector.py --evaluate normal --features model_google.pkl 

El siguiente ejemplo muestra como evaluar mediante la t�cnica de validaci�n cruzada el modelo **model_normal.pkl** incluido en el repositorio y que se ha generado sin tener en cuenta el servicio *GoogleSafeBrowsing*:

    $ python phishing_detector.py --evaluate cross --features model_normal.pkl 
---

## An�lisis de una URL

Permite clasificar una direcci�n URL en leg�tima o maliciosa a partir de la puntuaci�n de clasificaci�n que le otorga el algoritmo y que se obtiene del vector de caracter�sticas de la p�gina web analizada y del modelo de clasificaci�n generado con anterioridad.

Para el an�lisis es necesario invocar el programa con los siguientes par�metros:

 - **analyze_site**
 Contiene una direccion URL con el formato definido en el [RFC3986](https://tools.ietf.org/html/rfc3986).

 - **features**
 Nombre del fichero que contiene el vector de caracter�sticas de todas las URLs analizadas y que se ha generado previamente mediante la opcion *--generate_model*.

  - **model_type**
 Este parametro admite dos valores: **normal** o **google**.
 Especifica el tipo de modelo que se va a utilizar para el an�lisis de la URL. Si se invoca el programa con *--model_type normal* o se omite el par�metro, se realiza una extracci�n del vector de caracter�sitcas de la URL sin tener en cuenta la API de [Google Safe Browsing](https://developers.google.com/safe-browsing/v4/lookup-api). En caso de invocar el programa con *--model_type google* es necesario incluir el par�metro **api_key**. 
 
 - **api_key**
 Clave de desarrollador que facilita Google para invocar al servicio [Google Safe Browsing](https://developers.google.com/safe-browsing/v4/lookup-api). Para obtener una API_KEY es necesario registrarse de manera gratuita en el progama de [desarrolladores de Google](https://console.developers.google.com/).

El siguiente ejemplo muestra como analizar una direcci�n URL utilizando el modelo **model_normal.pkl** incluido en el repositorio:

    $ python phishing_detector.py --analyze_site 'https://duckduckgo.com/' --features model_normal.pkl 

El siguiente ejemplo muestra como analizar una direcci�n URL utilizando el modelo **model_google.pkl** incluido en el repositorio:

    $ python phishing_detector.py --analyze_site 'https://duckduckgo.com/' --features model_google.pkl --model_type google --api_key <API_KEY>