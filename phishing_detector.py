import os
import re
import csv
import sys
import json
import numpy
import whois
import pickle
import random
import socket
import ipinfo
import argparse
import datetime
import requests
import tldextract
import pandas as pd
import seaborn as sns
from sklearn import metrics
from pandas import DataFrame
from matplotlib import pyplot
from bs4 import BeautifulSoup
from urllib.parse import urlparse
from sklearn.model_selection import KFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

#CONSTANTS
MAX_URLS = 10000
NUM_TREES = 64 # number of trees in the forest.
IP_INFO_API_KEY = "ed7a7b5c64d6d3" # Generate one in https://ipinfo.io/
GOOGLE_API_KEY = "" # Generate one in https://cloud.google.com/
COUNTRY_CODES = ['ac','ad','ae','af','ag','ai','al','am','ao','aq','ar','as','at','au','aw','ax','az','ba','bb','bd','be','bf','bg','bh','bi','bj','bm','bn','bo','br','bs','bt','bw','by','bz','ca','cc','cd','cf','cg','ch','ci','ck','cl','cm','cn','co','cr','cu','cv','cw','cx','cy','cz','de','dj','dk','dm','do','dz','ec','ee','eg','er','es','et','eu','fi','fj','fk','fm','fo','fr','ga','gd','ge','gf','gg','gh','gi','gl','gm','gn','gp','gq','gr','gs','gt','gu','gw','gy','hk','hm','hn','hr','ht','hu','id','ie','il','im','in','io','iq','ir','is','it','je','jm','jo','jp','ke','kg','kh','ki','km','kn','kp','kr','kw','ky','kz','la','lb','lc','li','lk','lr','ls','lt','lu','lv','ly','ma','mc','md','me','mg','mh','mk','ml','mm','mn','mo','mp','mq','mr','ms','mt','mu','mv','mw','mx','my','mz','na','nc','ne','nf','ng','ni','nl','no','np','nr','nu','nz','om','pa','pe','pf','pg','ph','pk','pl','pm','pn','pr','ps','pt','pw','py','qa','re','ro','rs','ru','rw','sa','sb','sc','sd','se','sg','sh','si','sk','sl','sm','sn','so','sr','ss','st','su','sv','sx','sy','sz','tc','td','tf','tg','th','tj','tk','tl','tm','tn','to','tr','tt','tv','tw','tz','ua','ug','uk','us','uy','uz','va','vc','ve','vg','vi','vn','vu','wf','ws','ye','yt','za','zm','zw']
SUSPICIOUS_WORDS = ['webscr', 'secure', 'banking', 'ebayisapi', 'account', 'confirm', 'login', 'signin']


def get_features(url, googleSafe=False):
    #RESET FEATURE VALUES 
    at = 0                      # @  Char
    dot_meter = 0               # .  Char >=5 reps
    double_slash = 0            # // Char
    ip_instead_dn = 0           # Domain name is an IP
    port_in_url = 0             # Port number in the URL
    only_one_TLD = 0            # Only one TOP LEVEL DOMAIN
    domain_creation_date = 0    # Domain creation date is lesser than 20 days
    country_code = 0            # Domain country = IP gelocation country
    http_repeated = 0           # HTTP word appears more than once
    word_repeated = 0           # Word appears more than once in the URL
    form_action = 0             # Action URLs points to a different domain, or is empty or eq to "about:blank"
    suspicious_word = 0         # Suspicious words when form part of the URL
    # New features
    long_url = 0                # If len(URL)<54 => legit | else => phishing 
    short_url = 0               # If URL is shortened with an url shortener service
    dash_symbol = 0             # If dash symbol appears in the domain name
    domain_expires_soon = 0     # If domain expires on <= 180 dias
    favicon = 0                 # Favicon href points to another domain
    iframe = 0                  # HTML contain an iframe 
    googlesafe = 0              # If googlesafebrowsing marks site as phishing
    
    print(url)

    # Http ocurrences
    if url.count('http') > 1:
        http_repeated = 1
    
    # URL length
    if len(url) >= 54:
        long_url = 1
    
    url_shortener_services = ['tinyurl.com','bit.ly','cutt.ly','shorturl.me' ,'shorturl.at']
    for s in url_shortener_services:
        if url.count(s) >= 1:
            short_url = 1

    # dash symbol
    if url.count('-') >= 1:
        dash_symbol = 1

    # Repeated words
    aux_url = url.replace('https://', '').replace('http://','').replace('/','')
    words = aux_url.split('.')
    joined_words = ''.join(words)
    for w in words:
        if joined_words.count(w) > 1: 
            word_repeated = 1

    # Suspicious words
    for w in SUSPICIOUS_WORDS:
        if w in url:
            suspicious_word = 1

    # At char
    if "@" in url:
        at = 1
    
    #dot char
    count = 0
    for i in url:
        if i == '.':
            count = count + 1
    if count >= 5:
        dot_meter = 1

    # Double slash
    aux_url = url.replace('http://', '')
    aux_url = aux_url.replace('https://', '')
    if "//" in aux_url:
        double_slash = 1

    # IP instead of DNS
    aux_url = urlparse(url)
    if aux_url.hostname.replace(".","").isdigit():
        ip_instead_dn = 1

    # Port number in the URL
    if aux_url.port:
        port_in_url = 1

    # Only one TLD in the URL
    tld = tldextract.extract(url)
    count = 0
    for i in tld.suffix:
        if i == '.':
            count = count + 1
    if count >= 1:
        only_one_TLD = 1

    # Domain creation date is lesser than 20 days
    w = whois.whois(tld.registered_domain)
    now = datetime.datetime.now()
    creation = None
    expiration = None

    if type(w.creation_date) is list:
        creation = w.creation_date[0]
    else:
        creation = w.creation_date
    
    if type(creation) is datetime.datetime:
        delta = now - creation
        if delta.days <= 20:
            domain_creation_date = 1

    # Domain expiration date is less than 180 days
    if type(w.expiration_date) is list:
        expiration = w.expiration_date[0]
    else:
        expiration = w.expiration_date
    
    if type(expiration) is datetime.datetime:
        delta = now - expiration
        if delta.days <= 180:
            domain_expires_soon = 1

    # Domain country = IP gelocation country
    # Get IP from domain name
    name = '.'.join(part for part in tld if part)
    ip_address = socket.gethostbyname(name)
    # Get country code from IP
    access_token = IP_INFO_API_KEY
    handler = ipinfo.getHandler(access_token)
    details = handler.getDetails(ip_address)
    ip_country = details.country.upper()
    # Get country code from domain name
    spl = tld.suffix.split('.')
    last_suffix = spl[-1].upper()
    if last_suffix in COUNTRY_CODES:
        # The last suffix is a country code top-level domain    
        if ip_country != last_suffix:
            country_code = 1

    # Looking for forms in html content
    response = requests.get(url, timeout=10)
    if response.status_code == 200:
        html = response.text
        if '<form ' in html:
            # Check action attribute for each form
            bs = BeautifulSoup(html, 'html.parser')
            forms = bs.find_all('form')
            for f in forms:
                # Check absolute URLs in action attribute
                if 'action' in f:
                    if f.get('action').count('http') >= 1:
                        # Check if action URL points to another domain
                        if not tld.registered_domain in f.get('action'):
                            form_action = 1
                    if f.get('action') == '' or f.get('action') == 'about:blank':
                        form_action = 1
        # Iframe is used
        if '<iframe ' in html:
            iframe = 1
        # Favicon points to another domain
        if '<link ' in html:
            # Check action attribute for each link
            bs = BeautifulSoup(html, 'html.parser')
            links = bs.find_all('link')
            for l in links:
                if l.get('rel').count('icon') >= 1:
                    # Check if href points to another domain
                    if not tld.registered_domain in l.get('href'):
                        favicon = 1


    # store string features in dictionary form
    features = {  
        'http_repeated': http_repeated, 
        'word_repeated': word_repeated,
        'at': at, 
        'dot_meter': dot_meter,
        'double_slash': double_slash,
        'ip_instead_dn': ip_instead_dn,
        'port_in_url': port_in_url,
        'only_one_TLD': only_one_TLD,
        'domain_creation_date': domain_creation_date,
        'country_code': country_code,
        'form_action': form_action,
        'suspicious_word': suspicious_word,
        'long_url' : long_url,                
        'short_url' : short_url,             
        'dash_symbol' : dash_symbol,           
        'domain_expires_soon' : domain_expires_soon,    
        'favicon' : favicon,     
        'iframe' : iframe             
    }
    if googleSafe:
        features['googlesafe'] = 0

    print(features) 
    return features

def generate_model(legit_sites, phishing_sites, features_file):
    X, y = get_training_data(legit_sites,phishing_sites)
    
    # Saving data for next time
    pickle.dump((X,y),open(features_file,"wb+"))
    print("Saved features for next time in file: " + features_file)


def generate_model_googlesafebrowser(legit_sites, phishing_sites, features_file):
    X, y = get_training_data(legit_sites,phishing_sites,googleSafe=True)
    # Include info retrieved by the GoogleSafeBrowsing API for each URL as a new feature
    max_google_urls = 400
    urls = []
    results = []

    for url in X:
        urls.append(url)
        if len(urls) >= max_google_urls:
            results.append(googlesafebrowsingAPI(urls))
            urls = []
    if len(urls)>0:
        results.append(googlesafebrowsingAPI(urls))       

    for res in results:
        if 'matches' in res:
            matches = res['matches']
            for phish in matches:
                X[phish['threat']['url']]['googlesafe'] = 1

    X2 = []
    for x in X.values():
        X2.append(x)

    
    # Saving data for next time
    pickle.dump((X2,y),open(features_file,"wb+"))
    print("Saved features for next time in file: " + features_file)


def googlesafebrowsingAPI(urls):
    SB_CLIENT_ID = "Python SafeBrowsing Client"
    SB_CLIENT_VER = "0.0.1"

    apiurl = 'https://safebrowsing.googleapis.com/v4/threatMatches:find?key=%s' % (GOOGLE_API_KEY)
    platform_types = ['ANY_PLATFORM']
    threat_types = ['THREAT_TYPE_UNSPECIFIED',
                    'MALWARE', 
                    'SOCIAL_ENGINEERING', 
                    'UNWANTED_SOFTWARE', 
                    'POTENTIALLY_HARMFUL_APPLICATION']
    threat_entry_types = ['URL']
    threat_entries = []
    results = {}

    for url_ in urls: 
        url = {'url': url_} 
        threat_entries.append(url)

    reqbody = {
        'client': {
                'clientId': SB_CLIENT_ID,
                'clientVersion': SB_CLIENT_VER
        },
        'threatInfo': {
            'threatTypes': threat_types,
            'platformTypes': platform_types,
            'threatEntryTypes': threat_entry_types,
            'threatEntries': threat_entries
        }
    }
    
    headers = {'Content-Type': 'application/json'}
    r = requests.post(
            apiurl, 
            data=json.dumps(reqbody), 
            headers=headers)
    return r.json()

def get_training_data(legit_sites,phishing_sites, googleSafe=None):
    def get_training_urls(csv_file, max_urls):
        urls = []

        with open(csv_file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if not row[0] in urls:
                    urls.append(row[0])
                    line_count += 1
                    if line_count >= max_urls:
                        break
            print(" {0} - Processed {1} URLs.".format(csv_file.name, line_count))
        return urls

    phishing = get_training_urls(phishing_sites, max_urls=MAX_URLS)
    legit = get_training_urls(legit_sites, max_urls=MAX_URLS)
    
    if googleSafe:
        X = {}
        for url in phishing + legit:
            try:
                X[url] = get_features(url, googleSafe)
            except Exception as e:
                print("Error in extracting url features for: " + url + "\n Exception:" + str(e)) 
                if url in phishing: phishing.remove(url)
                elif url in legit: legit.remove(url)

        y = [1 for i in range(len(phishing))] + [0 for i in range(len(legit))]
        print("*********************************************************************************************************")
        print("Features extracted in dict form for " + str(len(phishing)) + " phishing sites and " + str(len(legit)) + " legit sites.")
        return X, y

    else:
        X = []
        for url in phishing + legit:
            try:
                X.append(get_features(url))
            except Exception as e:
                print("Error in extracting url features for: " + url + "\n Exception:" + str(e)) 
                if url in phishing: phishing.remove(url)
                elif url in legit: legit.remove(url)

        y = [1 for i in range(len(phishing))] + [0 for i in range(len(legit))]
        print("*********************************************************************************************************")
        print("Features extracted for " + str(len(phishing)) + " phishing sites and " + str(len(legit)) + " legit sites.")
        return X, y


def analyze_url(url, features_file, googleSafe=False):
    # scan a file to determine if it is malicious or benign
    if not os.path.exists(features_file):
        print("Train a detector before scanning sites.")
        sys.exit(1)
    with open(features_file, 'rb') as saved_features:
        X, y = pickle.load(saved_features)
    
    try:
        features = get_features(url, googleSafe)
        if googleSafe:
            urls=[]
            urls.append(url)
            res=googlesafebrowsingAPI(urls)
            if 'matches' in res:
                features['googlesafe'] = 1
                print('googlesafe = 1')

    except Exception as e:
        print("Error in extracting url features for: " + url + "\n Exception:" + str(e)) 
        print("Please, try with another URL") 
        exit(1)

    # Create our classifier
    v = DictVectorizer(sparse=True)
    X = v.fit_transform(X)

    classifier = RandomForestClassifier(NUM_TREES)
    classifier.fit(X,y)
    
    x1=v.fit_transform([features])

    result_proba = classifier.predict_proba(x1)[:,-1]
    # if the user specifies phishing_sites and
    # legit_sites, train a detector
    if result_proba > 0.5:
        print("It appears this site is malicious! " + str(result_proba))
    else:
        print("It appears this site is legit. " + str(result_proba))


def evaluate(model_file):
    if not os.path.exists(model_file):
        print("Train a detector before trying to evaluate it!")
        sys.exit(1)
    with open(model_file, 'rb') as saved_features:
        X, y = pickle.load(saved_features)

    print("Working with a total of " + str(len(X)) + ", " + str(y.count(1)) + " phishing and " + str(y.count(0)) + " legit sites.")
    plotCorrelationHeatmap(X,y)
    X, y = numpy.array(X), numpy.array(y)
    indices = list(range(len(y)))
    # Randomly assign samples to training and test sets
    random.shuffle(indices)
    X, y = X[indices], y[indices]

    splitpoint = int(len(X)*80/100) 
    # Split data into training and test sets
    training_X, test_X = X[:splitpoint], X[splitpoint:]
    training_y, test_y = y[:splitpoint], y[splitpoint:]
    # Prepare data for the classifier
    v = DictVectorizer(sparse=True)
    training_X = v.fit_transform(training_X)
    test_X = v.fit_transform(test_X)

    classifier = RandomForestClassifier(NUM_TREES)
    classifier.fit(training_X,training_y)
    # Store only the phishing probability
    scores = classifier.predict_proba(test_X)[:,-1]
    # Compute ROC curve
    fpr, tpr, thresholds = metrics.roc_curve(test_y, scores)

    # Visualize ROC curve
    pyplot.plot(fpr,tpr,'r-')
    pyplot.xlabel("Detector false positive rate")
    pyplot.ylabel("Detector true positive rate")
    pyplot.title("Detector ROC Curve")
    pyplot.show()

    printEvaluation(classifier, test_X, test_y, scores)


def cv_evaluate(model_file):
    if not os.path.exists(model_file):
        print("Train a detector before trying to evaluate it!")
        sys.exit(1)
    with open(model_file, 'rb') as saved_features:
        X, y = pickle.load(saved_features)
    
    print("Working with a total of " + str(len(X)) + ", " + str(y.count(1)) + " phishing and " + str(y.count(0)) + " legit sites.")
    
    # Convert training data into numpy arrays
    X, y = numpy.array(X), numpy.array(y)
    # Start cross-validation
    fold_counter = 0
    kf = KFold(n_splits=5,shuffle=True)
    for train, test in kf.split(X):
        training_X, training_y = X[train], y[train]
        test_X, test_y = X[test], y[test]
        # Prepare data for the classifier
        v = DictVectorizer(sparse=True)
        training_X = v.fit_transform(training_X)
        test_X = v.fit_transform(test_X)

        # Instantiate and train the RandomForestClassifier
        classifier = RandomForestClassifier(NUM_TREES)
        classifier.fit(training_X,training_y)
        # Compute a ROC curve for this particular fold 
        # and then plot a line that represents this ROC curve
        scores = classifier.predict_proba(test_X)[:,-1]
       
        fpr, tpr, thresholds = metrics.roc_curve(test_y, scores)
        pyplot.semilogx(fpr,tpr,label="Fold number {0}".format(fold_counter))
        fold_counter += 1
        
        printEvaluation(classifier, test_X, test_y, scores)

    # When each fold is processed, display the chart
    pyplot.xlabel("Detector false positive rate")
    pyplot.ylabel("Detector true positive rate")
    pyplot.title("Detector Cross-Validation ROC Curves")
    pyplot.legend()
    pyplot.grid()
    pyplot.show()


def printEvaluation(classifier, test_X, test_y, scores):
    print('Mean Absolute Error:' + str(metrics.mean_absolute_error(test_y, scores)))
    print('Mean Squared Error:' + str(metrics.mean_squared_error(test_y, scores)))
    print('Root Mean Squared Error:' + str(numpy.sqrt(metrics.mean_squared_error(test_y, scores))))

    y_pred_test = classifier.predict(test_X)
    print("Confusion Matrix:\n")
    print(confusion_matrix(test_y,y_pred_test))
    print("Classification Report:\n")
    print(classification_report(test_y,y_pred_test))
    print("Accuracy_score: " + str(accuracy_score(test_y, y_pred_test)))

def plotCorrelationHeatmap(X,y):
    df = DataFrame(X)
    df['result'] = y
    print(df.head())
    print(df.info())
    print(df['result'].value_counts())
    pyplot.clf()
    sns.heatmap(df.corr(), linewidths=.5, cmap="GnBu", annot=True)
    pyplot.show()


parser = argparse.ArgumentParser("")
parser.add_argument("--generate_model",choices=['normal', 'google'], help="Generate one of the models")
parser.add_argument("--api_key",default=None,help="Google Safe Browsing API")
parser.add_argument("--phishing_sites",default=None,help="CSV phishing training URLs")
parser.add_argument("--legit_sites",default=None,help="CSV legitimate training URLs")
parser.add_argument("--analyze_site",default=None,help="URL to analyze")
parser.add_argument("--features",default=None,help="Features array filename. If exists opens. If not, creates it")
parser.add_argument("--evaluate",  choices=['normal', 'cross'], help="Perform normal evaluation or crossvalidation")
parser.add_argument("--max_urls",default=10000,help="Max number of URL to analyze from each CSV files")     
parser.add_argument("--model_type",choices=['normal', 'google'], help="Features model type")

args = parser.parse_args()

if args.max_urls:
    MAX_URLS = int(args.max_urls)

if args.analyze_site:
    if not args.features:
        print("You must indicate a valid features file with the --features argumet")
        sys.exit(0)
    if not args.model_type:
        print("Analyzing URL in normal mode...")
        analyze_url(args.analyze_site, args.features)
    elif args.model_type == 'normal':
        print("Analyzing URL in normal mode...")
        analyze_url(args.analyze_site, args.features)
    elif args.model_type == 'google':
        if not args.api_key:
            print("If you want to analyze an URL with this characteristic you must provide your GoogleSafeBrowsing API_KEY")
            sys.exit(1)
        else:
            GOOGLE_API_KEY = args.api_key
            print("Analyzing URL with GoogleSafeBrowsing...")
            analyze_url(args.analyze_site, args.features, googleSafe=True)

elif args.generate_model:
    if not args.phishing_sites or not args.legit_sites or not args.features:
        print("If you want to train a model you must specificate the following parameters:")
        print("  phishing_sites, legit_sites, features")
        print("See help with --help for more info")
    else:
        print("Generating model...")
        print("Maximum number of URLs from each CSV => " + str(args.max_urls))
        if args.generate_model == 'normal':
            generate_model(args.legit_sites,args.phishing_sites, args.features)
        elif args.generate_model == 'google':
            if not args.api_key:
                print("If you want to generate this model must provide your GoogleSafeBrowsing API_KEY")
                sys.exit(1)
            else:
                GOOGLE_API_KEY = args.api_key
                generate_model_googlesafebrowser(args.legit_sites, args.phishing_sites, args.features)
    

elif args.evaluate and args.features:
    print("Evaluating...")
    if args.evaluate == 'normal':
        evaluate(args.features)
    elif args.evaluate == 'cross':
        cv_evaluate(args.features)



